package pamoka;

public class LabasVardas {
	String vardas;
	int metai;
	String darbas;
	public LabasVardas(String vardas,int metai, String darbas) {
		this.vardas = vardas;
		
		this.metai = metai;
		this.darbas = darbas;
	}
	
public static void main(String[] args) {
	LabasVardas vardelis = new LabasVardas("Minde", 36,"traktoristas");
	System.out.println("Zdrf "+ vardelis.vardas + " tau yra " + vardelis.metai + " ir tavo profesija yra: "+ vardelis.darbas);
}

}
